<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $USER;

$arResult = array();

$obCache = new CPHPCache();
$cacheLifetime = $arParams['CACHE_TIME'];
$cacheID = "FURPUR_SERVICE_COMMENTS_".$arParams['SERVICE_ID'];
$cachePath = SITE_ID.'/catalog/detail/comments/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
	$arVars = $obCache->GetVars();
	$arResult = $arVars['arResult'];
} elseif($obCache->StartDataCache()) {

	if ($arParams['SERVICE_ID']) {

		// Comments
		$arComments = array();
		$arUserId = array();
		$arFilter = array(
			'IBLOCK_ID' => FURPUR_COMMENTS_IBLOCK_ID,
			'PROPERTY_SERVICE' => $arParams['SERVICE_ID'],
			'ACTIVE' => 'Y',
		);

		$arSelect = array(
			'ID',
			'IBLOCK_ID',
			'NAME',
			'ACTIVE_FROM',
			'DETAIL_TEXT',
			'PROPERTY_USER',
			'PROPERTY_AUTHOR_NAME',
			'PROPERTY_PARENT',
			'PROPERTY_IS_ANSWER',
		);

		$dbRes = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($res = $dbRes->Fetch()) {

			$arTemp = array();
			$arTemp['ID'] = $res['ID'];
			$arTemp['AUTHOR_NAME'] = $res['PROPERTY_AUTHOR_NAME_VALUE'];
			$arTemp['DATE'] = ConvertTimeStamp(MakeTimeStamp($res['ACTIVE_FROM']), 'SHORT');
			$arTemp['TITLE'] = $res['PROPERTY_TITLE_VALUE'];
			$arTemp['TEXT'] = $res['DETAIL_TEXT'];
			$arTemp['USER_ID'] = $res['PROPERTY_USER_VALUE'];
			$arTemp['PARENT'] = $res['PROPERTY_PARENT_VALUE'];
			$arTemp['IS_ANSWER'] = $res['PROPERTY_IS_ANSWER_VALUE'];

			$arComments[] = $arTemp;
			if ($res['PROPERTY_USER_VALUE']){
				$arUserId[] = $res['PROPERTY_USER_VALUE'];
			}
		}

		// Pictures
		$arImages = array();
		$arFilter = array(
			'IBLOCK_ID' => FURPUR_IMAGES_IBLOCK_ID,
		);

		$arSelect = array(
			'ID',
			'IBLOCK_ID',
			'CODE',
			'DETAIL_PICTURE',
		);

		$dbRes = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($res = $dbRes->Fetch()) {
			$arImages[$res['CODE']] = $res['DETAIL_PICTURE'];
		}
		$arResult['IMAGES'] = $arImages;

		// Users
		if ($arParams['USER_ID']) {
			$arUserId[] = $arParams['USER_ID'];
		}

		$arUsers = array();
		if ($arUserId) {

			$arFilter = array(
				'ID' => implode('|',$arUserId),
			);

			$arSelect = array(
				'FIELDS' => array(
					'ID',
					'NAME',
					'LAST_NAME',
					'PERSONAL_PHOTO',
				),
			);

			$dbRes = CUser::GetList( $by = '', $order = '', $arFilter, $arSelect);
			while ($res = $dbRes->Fetch()){

				$arTemp = array();

				$arTemp['USER_PICTURE'] = CFile::ResizeImageGet($arImages['user-default'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				if ($res['PERSONAL_PHOTO']) {
					$arTemp['USER_PICTURE'] = CFile::ResizeImageGet($res['PERSONAL_PHOTO'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				}

				$arTemp['NAME'] = $res['NAME'];
				if ($res['LAST_NAME']) {
					$arTemp['NAME'] .= " ".$res['LAST_NAME'];
				}
				$arUsers[$res['ID']] = $arTemp;
			}
		}

		foreach ($arComments as $key => $comment) {

			$arComments[$key]['USER_PICTURE'] = CFile::ResizeImageGet($arImages['user-default'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true);

			if ($arUsers[$comment['USER_ID']]['USER_PICTURE']) {
				$arComments[$key]['USER_PICTURE'] = $arUsers[$comment['USER_ID']]['USER_PICTURE'];
			}

			if ($comment['IS_ANSWER'] == 'Y') {
				$arComments[$key]['USER_PICTURE'] = CFile::ResizeImageGet($arImages['furpur-default'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			}
		}

		$tree = getChilds(0, $arComments);

		// $arResult
		$arResult['SERVICE'] = array(
			'ID' => intval($arParams['SERVICE_ID']),
		);

		$arResult['COMMENTS'] = $tree;

		$obCache->EndDataCache(array(
			'arResult' => $arResult,
		));

	} else {
		$obCache->AbortDataCache();
	}
}

// current user (no cache!)
if ($USER->IsAuthorized()) {

	$currentUserId = $USER->GetId();

	if ($currentUserId) {

		$arCurrentUser = array();

		$arFilter = array(
			'ID' => $currentUserId,
		);

		$arSelect = array(
			'FIELDS' => array(
				'ID',
				'NAME',
				'LAST_NAME',
				'PERSONAL_PHOTO',
			),
		);

		$dbRes = CUser::GetList( $by = '', $order = '', $arFilter, $arSelect);
		if ($res = $dbRes->Fetch()){

			$arCurrentUser = array();

			$arCurrentUser['USER_PICTURE'] = CFile::ResizeImageGet($arResult['IMAGES']['user-default'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			if ($res['PERSONAL_PHOTO']) {
				$arCurrentUser['USER_PICTURE'] = CFile::ResizeImageGet($res['PERSONAL_PHOTO'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			}

			$arCurrentUser['ID'] = $res['ID'];

			$arCurrentUser['NAME'] = $res['NAME'];
			if ($res['LAST_NAME']) {
				$arCurrentUser['NAME'] .= " ".$res['LAST_NAME'];
			}

			$arResult['USER'] = $arCurrentUser;

		}
	}

} else {

	$arResult['USER'] = array(
		'NAME' => "Гость",
		'USER_PICTURE' => CFile::ResizeImageGet($arResult['IMAGES']['user-default'], array('width' => 36, 'height' => 36), BX_RESIZE_IMAGE_PROPORTIONAL, true),
	);

}

$this->IncludeComponentTemplate();


// childs
function getChilds($parent, $array)
{
	$out = array();
	foreach ($array as $item) {
		if($item['PARENT'] == $parent) {
			$arTemp = $item;
			$arTemp['CHILDS'] = getChilds($item['ID'], $array);
			$out[] = $arTemp;
		}
	}
	return $out;
}
