<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

// English version
if (SITE_ID == EN_VERSION_SITE_ID) {

	// fields to translate
	$arTranslateFields = array(
		'ID',
		'NAME',
		'PREVIEW_TEXT',
		'DETAIL_TEXT',
		'TAGS',
	);

	// properties to translate
	foreach ($arResult['PROPERTIES'] as $property) {
		if ($property['PROPERTY_TYPE'] == 'S' && (empty($property['USER_TYPE']) || $property['USER_TYPE'] == 'HTML')) {
			$arTranslateFields[] = 'PROPERTY_' . $property['CODE'];
		}
	}

	// get translations
	$arTranslated = array();

	$arOrder = array();

	$arFilter = array(
		'IBLOCK_ID' => CATALOG_EN_IBLOCK_ID,
		'PROPERTY_ORIGIN_LINK' => $arResult['ID'],
	);

	$arSelect = array(
		'PROPERTY_ORIGIN_LINK',
	);
	$arSelect = array_merge($arSelect, $arTranslateFields);

	$dbRes = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	if ($res = $dbRes->GetNext()) {
		$arTranslated = $res;
	}

	// seo
	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(CATALOG_EN_IBLOCK_ID, $arTranslated['ID']);
	$arSeo = $ipropValues->getValues();
	$arTranslated['IPROPERTY_VALUES'] = $arSeo;
	unset($arTranslated['ID']);
	unset($arTranslated['~ID']);

	// display property names
	$displayPropertyNames = array();
	$arFilter = array(
		'IBLOCK_ID' => CATALOG_EN_IBLOCK_ID,
	);

	$dbRes = CIBlockProperty::GetList(array(), $arFilter);
	while ($res = $dbRes->GetNext()) {
		if (!empty($arResult['DISPLAY_PROPERTIES'][$res['CODE']])) {
			$arResult['DISPLAY_PROPERTIES'][$res['CODE']]['NAME'] = $res['NAME'];
			$arResult['DISPLAY_PROPERTIES'][$res['CODE']]['~NAME'] = $res['~NAME'];
		}
	}

	// references
	foreach ($arResult['DISPLAY_PROPERTIES'] as $displayPropertyKey => $displayProperty) {

		if ($displayProperty['PROPERTY_TYPE'] == 'E' && !empty($displayProperty['VALUE'])) {

			if (is_array($displayProperty['VALUE'])) {
				$refValueId = array();
				foreach ($displayProperty['VALUE'] as $id) {
					$refValueId[] = $id;
				}
			} else {
				$refValueId = $displayProperty['VALUE'];
			}

			$arOrder = array();

			$arFilter = array(
				'IBLOCK_ID' => $displayProperty['LINK_IBLOCK_ID'],
				'ID' => $refValueId,
			);

			$arSelect = array(
				'NAME',
				'PROPERTY_NAME_EN',
			);

			$translatedDisplayValue = array();
			$dbRes = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
			while ($res = $dbRes->GetNext()) {

				if (!empty($res['~PROPERTY_NAME_EN_VALUE'])) {
					$translatedDisplayValue[] = $res['~PROPERTY_NAME_EN_VALUE'];
				} else {
					$tr = new CustomTranslate();
					$translatedDisplayValue[] = $tr->Translate($res['~NAME'], 'ru', 'en');
				}
			}

			$arResult['DISPLAY_PROPERTIES'][$displayPropertyKey]['DISPLAY_VALUE'] = $translatedDisplayValue;
		}
	}

	// apply translations
	if (!empty($arTranslated)) {

		// fields
		foreach ($arResult as $field => $value) {
			if (!empty($arTranslated[$field])) {
				$arResult[$field] = $arTranslated[$field];
			}
		}

		// properties
		foreach ($arResult['PROPERTIES'] as $propertyName => $property) {
			if (!empty($arTranslated['PROPERTY_' . $propertyName . '_VALUE'])) {
				$arResult['PROPERTIES'][$propertyName]['VALUE'] = $arTranslated['PROPERTY_' . $propertyName . '_VALUE'];
				$arResult['PROPERTIES'][$propertyName]['~VALUE'] = $arTranslated['~PROPERTY_' . $propertyName . '_VALUE'];
				if (!empty($arResult['DISPLAY_PROPERTIES'][$propertyName])) {
					$arResult['DISPLAY_PROPERTIES'][$propertyName]['DISPLAY_VALUE'] = $arTranslated['PROPERTY_' . $propertyName . '_VALUE'];
					$arResult['DISPLAY_PROPERTIES'][$propertyName]['~DISPLAY_VALUE'] = $arTranslated['~PROPERTY_' . $propertyName . '_VALUE'];
				}
			}
		}

		// seo
		$arResult['IPROPERTY_VALUES'] = $arTranslated['IPROPERTY_VALUES'];
	}
}
