$(document).ready(function()
    {
        // mask phone
        $('.mask-phone' ).mask('9999999999');

        // auth form
        $('#auth-form').submit(function()
        {
            var form = $(this);

            var result = checkAllFormValids(form, '.popup-error');
            if (result == true){

                var url = form.attr('action');
                var login = form.find('input[name=login-contact]').val();
                var password = form.find('input[name=login-pass]').val();

                $.post(
                    url,
                    {
                        LOGIN: login,
                        PASSWORD: password
                    },
                    function(data)
                    {
                        var currentError;
                        if (data == 'successAuth') {
                            $('.md-overlay').click();
                            location.reload();

                        } else if (data == 'userNotExists'){
                            form.find('input[name=login-contact]').addClass('error');
                            currentError = form.find('input[name=login-contact]').parent().find('.popup-error');
                            currentError.html('Пользователь не найден');
                            currentError.addClass('active');
                        } else if (data == 'userWrongPass'){
                            form.find('input[name=login-pass]').addClass('error');
                            currentError = form.find('input[name=login-pass]').parent().find('.popup-error');
                            currentError.html('Неверный пароль');
                            currentError.addClass('active');
                        }
                    }
                );

            }
            return false;
        });

        // register form
        $('#reg-form').submit(function()
        {
            var form = $(this);

            var result = checkAllFormValids(form, '.popup-error');
            if (result == true){

                var url = form.attr('action');
                var name = form.find('input[name=reg-name]').val();
                var login = form.find('input[name=reg-contact]').val();
                var password = form.find('input[name=reg-pass]').val();

                $.post(
                    url,
                    {
                        NAME: name,
                        LOGIN: login,
                        PASSWORD: password
                    },
                    function(data){

                        var currentError;
                        if (data == 'successReg') {
                            $('.md-overlay').click();
                            location.reload();

                        } else if (data == 'userExists'){
                            form.find('input[name=reg-contact]').addClass('error');
                            currentError = form.find('input[name=reg-contact]').parent().find('.popup-error');
                            currentError.html('Пользователь с таким e-mail или телефоном уже зарегистрирован');
                            currentError.addClass('active');
                        }
                    }
                );
            }
            return false;
        });

        // forget password form
        $('#forget-password-form').submit(function()
        {
            var form = $(this);

            var result = checkAllFormValids(form, '.popup-error');
            if (result == true){

                var url = form.attr('action');
                var contact = form.find('input[name=recovery-contact]').val();

                $.post(
                    url,
                    {
                        CONTACT: contact
                    },
                    function ( data )
                    {
                        if (data == 'userNotExists'){

                            form.find('input[name=recovery-contact]')
                                .parent()
                                .find('.popup-error')
                                .html('Пользователь не найден');

                            form.find('input[name=recovery-contact]')
                                .parent()
                                .find('.popup-error')
                                .addClass('active');
                        }
                        else if (data == 'successRecovery'){
                            $('.md-overlay').click();
                        }
                    }
                );
            }
            return false;
        });


        // social networks
        $('a.socnet-link').on('click', function(e)
            {
                e.preventDefault();
                $('div.md-close').click();
                BxShowAuthFloat($(this).attr('data-service'), $(this).attr('data-suffix'));
            }
        );

        // logout link
        $('#top-user-links').on('click', 'a.logout-link', function(e)
            {
                e.preventDefault();
                $.post(
                    '/ajax/logout.php',
                    function (data)
                    {
                        location.reload();
                    }
                );
            }
        );

        // auth/register/forget-password remove errors
        $('#auth-form, #reg-form, #forget-password-form').on('focus', 'input', function(){
            $(this).removeClass('error');
            var currentLabel = $(this).parent();
            currentLabel.find('.popup-error').removeClass('active');
        });
    }
);


// functions

// проверка полей формы на стороне клиента
function checkAllFormValids(container, errorElement){

    // снятие классов ошибок
    container.find('input, textarea').removeClass('error');
    container.find(errorElement).removeClass('active');

    var isValid = 1;
    var type = 0;

    container.find('label').each(function(){

        $(this).removeClass('error');

        // для обязательных полей
        if ($(this).hasClass('required')) {

            if ($(this).find('input, textarea').val() <= 0) {
                $(this).find('input, textarea').addClass('error');
                isValid = 0;
            }
            else if ($(this).hasClass('contact')) {
                if ($(this).hasClass('email')){
                    type = 1;
                }
                else if ($(this).hasClass('phone')){
                    type = 2;
                }
                if (!validEmail($(this).find('input').val(),type)) {
                    $(this).find('input').addClass('error');
                    $(this).find(errorElement).addClass('active');
                    isValid = 0;
                }
            }

        }

    });

    container.find('label.required.error:first').find('input, textarea').focus();

    return (isValid) ? true : false;
}

// обновление малой корзины
function basketLineReload(){
    $.post(
        location.href,
        function(data){
            $('#top-basket-line').html($(data).find('#top-basket-line').html());
        }
    );
}